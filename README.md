# nuxeo-gcp

### 事前準備
- Cloud VPC
- Cloud NAT

### サービスアカウントの設定
env/credential.json にGCPサービスアカウントのjson形式ファイルを設置する  


### 実行

```
# 初期設定作成
terraform init

# 誤りチェック
terraform validate
terraform plan

# 実行
terraform apply -auto-approve

# リソース削除
terraform destory -auto-approve
```

サービスアカウントのAnsible用キー発行
```
# サービスアカウント用のSSH鍵を作成する
ssh-keygen -f env/id_rsa

# 作成したサービスアカウントのキーファイルを作成する
gcloud iam service-accounts keys create env/ansible.json --iam-account=ansible-provision@{Project ID}.iam.gserviceaccount.com

# 作成したサービスアカウントの認証を通す
gcloud auth activate-service-account --key-file=env/ansible.json

# 作成したSSH鍵の公開鍵をos-loginに登録する
gcloud compute os-login ssh-keys add --key-file=env/id_rsa.pub

# アカウントIDの取得
gcloud iam service-accounts describe ansible-provision@{Project ID}.iam.gserviceaccount.com --format='value(uniqueId)'

# 疎通確認
ssh -i env/id_rsa sa_00000@0.0.0.0

# Ansible実行テスト
ansible-playbook --list-hosts --inventory inventories/gcp.yml --private-key ../env/id_rsa --user connector --diff -v site.yml
```

### APIの有効化

Cloud SQL Admin API  
https://console.developers.google.com/apis/api/sqladmin.googleapis.com/overview  

Google Cloud Memorystore for Redis API  
https://console.cloud.google.com/marketplace/product/google/redis.googleapis.com/overview

Cloud Resource Manager API  
https://console.developers.google.com/apis/api/cloudresourcemanager.googleapis.com/overview
