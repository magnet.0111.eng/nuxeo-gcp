#
# SQL
#
resource "google_sql_database_instance" "master" {
    name             = "master-202010070000"
    database_version = "POSTGRES_9_6"
    region           = lookup(var.region, terraform.workspace)

    settings {
        tier = "db-f1-micro"
        ip_configuration {
          ipv4_enabled    = false
          private_network = "projects/${lookup(var.project_id, terraform.workspace)}/global/networks/${lookup(var.network, terraform.workspace)}"
        }
    }
}

resource "google_sql_database" "nuxeo" {
  name      = "nuxeo"
  instance  = google_sql_database_instance.master.name
}

resource "google_sql_user" "users" {
    name     = "nuxeo"
    instance = google_sql_database_instance.master.name
    host     = "*"
    password = "nuxeo"
}
