#
# Memorystore
#
resource "google_redis_instance" "cache" {
    name               = "cache-1"
    display_name       = "Nuxeo Cache 1"
    memory_size_gb     = 1
    redis_version      = "REDIS_4_0"
    authorized_network = lookup(var.network, terraform.workspace)
}
