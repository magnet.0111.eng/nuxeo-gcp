#
# GCE: Elasticsearch
#
resource "google_compute_instance" "elastic-node-1" {
    name          = "elastic-node-1"
    machine_type  = "n1-standard-1"
    zone          = lookup(var.region_zone, terraform.workspace)
    boot_disk {
        auto_delete = true
        initialize_params {
            image = "ubuntu-os-cloud/ubuntu-1804-lts"
            size = 10
            type = "pd-standard"
        }
    }

    network_interface {
        network = lookup(var.network, terraform.workspace)
    }

    metadata = {
        ssh-keys = lookup(var.ssh-key, terraform.workspace)
    }

    labels = {
      service_type = "es"
    }
}
