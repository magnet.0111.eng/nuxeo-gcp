#
# Provider
#
provider "google" {
    credentials = file(lookup(var.credential, "data"))
    project     = lookup(var.project_id, terraform.workspace)
    region      = lookup(var.region, terraform.workspace)
}
