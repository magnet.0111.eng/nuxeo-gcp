#
# Nuxeo Web
#
resource "google_service_account" "gce_iam" {
    account_id   = "gce-nuxeo-web"
    display_name = "gce-nuxeo-web"
    description  = "GCEにつけるサービスアカウント"
}

variable "gce_iam_roles" {
    default = [
        "roles/storage.admin"
    ]
}

resource "google_project_iam_member" "gce_iam" {
    count   = length(var.gce_iam_roles)
    role    = element(var.gce_iam_roles, count.index)
    member  = "serviceAccount:${google_service_account.gce_iam.email}"
}

variable "instance" {
    default = [
        "instance-1",
        "instance-2"
    ]
}

resource "google_compute_instance" "instance" {
    allow_stopping_for_update = true

    count         = length(var.instance)
    name          = element(var.instance, count.index)
    machine_type  = "n1-standard-1"
    zone          = lookup(var.region_zone, terraform.workspace)
    boot_disk {
        auto_delete = true
        initialize_params {
            image = "ubuntu-os-cloud/ubuntu-1804-lts"
            size = 10
            type = "pd-standard"
        }
    }

    network_interface {
        network = lookup(var.network, terraform.workspace)
    }

    service_account {
        email = google_service_account.gce_iam.email
        scopes = ["cloud-platform"]
    }

    metadata = {
        ssh-keys = lookup(var.ssh-key, terraform.workspace)
    }

    labels = {
      service_type = "web"
    }
}

resource "google_compute_instance_group" "nuxeo-group" {
    name        = "nuxeo-group"
    description = "nuxeo instance group"
    zone        = lookup(var.region_zone, terraform.workspace)

    named_port {
        name = "nuxeo"
        port = "8080"
    }

    instances = [
        google_compute_instance.instance[0].self_link,
        google_compute_instance.instance[1].self_link
    ]
}

#
# 負荷分散
#
resource "google_compute_health_check" "nuxeo-health-check" {
    name = "nuxeo-health-check"
    check_interval_sec = 10
    timeout_sec        = 3

    tcp_health_check {
        port = "8080"
    }
}

resource "google_compute_backend_service" "backend-service" {
    name        = "backend-service"
    port_name   = "nuxeo"
    protocol    = "HTTP"
    timeout_sec = 30

    backend {
        group = google_compute_instance_group.nuxeo-group.self_link
    }

    health_checks = [
        google_compute_health_check.nuxeo-health-check.self_link
    ]
}

resource "google_compute_url_map" "nuxeo-lb" {
    name            = "nuxeo-lb"
    default_service = google_compute_backend_service.backend-service.self_link
}

resource "google_compute_target_http_proxy" "target-http-proxy" {
     name    = "target-http-proxy"
     url_map = google_compute_url_map.nuxeo-lb.self_link
}

resource "google_compute_global_forwarding_rule" "global-forwarding-rule-http" {
    name       = "global-forwarding-rule-http"
    target     = google_compute_target_http_proxy.target-http-proxy.self_link
    port_range = "80"
}
