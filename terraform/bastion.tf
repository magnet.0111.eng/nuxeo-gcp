#
# GCE: bastion
#
resource "google_compute_instance" "bastion" {
    name          = "bastion"
    machine_type  = "f1-micro"
    zone          = lookup(var.region_zone, terraform.workspace)
    boot_disk {
        auto_delete = true
        initialize_params {
            image = "ubuntu-os-cloud/ubuntu-1804-lts"
            size = 10
            type = "pd-standard"
        }
    }

    network_interface {
        network = lookup(var.network, terraform.workspace)

        access_config {}
    }

    metadata = {
        enable-oslogin = true
    }

    labels = {
      service_type = "bastion"
    }
}
