#
# DNS
#
resource "google_dns_managed_zone" "nuxeo-zone" {
    name       = "nuxeo-zone"
    dns_name   = "nuxeo.private."
    visibility = "private"
    private_visibility_config {
        networks {
            network_url = "projects/${lookup(var.project_id, terraform.workspace)}/global/networks/${lookup(var.network, terraform.workspace)}"
        }
    }
}

resource "google_dns_record_set" "database_record" {
    managed_zone = google_dns_managed_zone.nuxeo-zone.name
    name         = "master-db.${google_dns_managed_zone.nuxeo-zone.dns_name}"
    type         = "A"
    ttl          = 300
    rrdatas      = [
        google_sql_database_instance.master.first_ip_address
    ]
}

resource "google_dns_record_set" "memorystore_record" {
    managed_zone = google_dns_managed_zone.nuxeo-zone.name
    name         = "cache.${google_dns_managed_zone.nuxeo-zone.dns_name}"
    type         = "A"
    ttl          = 300
    rrdatas      = [
        google_redis_instance.cache.host
    ]
}
