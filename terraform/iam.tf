#
# IAM
#
resource "google_service_account" "ansible_provision_account" {
    account_id   = "ansible-provision"
    display_name = "ansible-provision"
    description  = "ansibleのプロビジョニングで使用するためのサービスアカウント"
}

variable "ansible_provision_account_roles" {
    default = [
        "roles/compute.admin",
        "roles/iam.serviceAccountUser"   # GCE OSログイン用に追加している
    ]
}

resource "google_project_iam_member" "ansible_gcp_provisioning_compute_admin" {
    count   = length(var.ansible_provision_account_roles)
    role    = element(var.ansible_provision_account_roles, count.index)
    member  = "serviceAccount:${google_service_account.ansible_provision_account.email}"
}
